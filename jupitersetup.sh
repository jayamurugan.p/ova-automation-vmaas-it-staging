#!/bin/bash

#----------Phase 1----------
sudo useradd -m -s /bin/bash callhome
sudo mkdir /home/callhome/.ssh
ssh-keygen -t ed25519 -f /home/callhome/.ssh/id_ed25519 -N ''
sudo chown callhome /home/callhome/.ssh/id_ed25519



#----------Phase 2----------
echo "What is the port #?"
read PORT

CONNECT_COMMAND="openssl s_client -quiet -connect 50.216.117.76:443"

SCRIPT="#!/bin/bash
CONNECT_COMMAND=\"openssl s_client -quiet -connect 50.216.117.76:443\"
while : ; do
    ssh -N -v \\
        -R \"$PORT\":127.0.0.1:22 \\
        -o ConnectTimeout=60 \\
        -o ExitOnForwardFailure=true \\
        -o StrictHostKeyChecking=no \\
        -o UserKnownHostsFile=/dev/null \\
        -o ServerAliveInterval=30 \\
        -o ServerAliveCountMax=3 \\
        -o ProxyCommand=\"\$CONNECT_COMMAND\" \\
        -i \"/home/callhome/.ssh/id_ed25519\" \\
        -p 443 \\
    callhome@50.216.117.76
    sleep 30
done"

echo "$SCRIPT" > "/home/callhome/callhome-jupiter.sh"

sudo chmod +x /home/callhome/callhome-jupiter.sh



#----------Phase 3----------
cat > "/lib/systemd/system/callhome-jupiter.service" << EOL
[Unit]
Description=CSW Securin Callhome to Jupiter cluster

[Service]
ExecStart=/home/callhome/callhome-jupiter.sh
User=callhome

[Install]
WantedBy=default.target
EOL



#----------Phase 4----------
sudo systemctl daemon-reload
echo .
sleep 1
echo ..
sleep 1
echo ...
sleep 1
echo ....
sleep 1
echo .....
sleep 1
sudo systemctl enable --now callhome-jupiter.service
echo .
sleep 1
echo ..
sleep 1
echo ...
sleep 1
echo ....
sleep 1
echo .....
sleep 1
echo ......
sleep 1
echo .......
sleep 1
echo ........
echo ---------------------------------------------
echo ---Copy this key to the server.---
echo restrict,port-forwarding,permitlisten=\"$PORT\",permitopen=\"127.0.0.1:22\",command=\"/bin/false\" $(sudo cat /home/callhome/.ssh/id_ed25519.pub)
echo ---------------------------------------------
echo .
echo .
echo .
echo Copy the public key over to the server. Once you have done that you can run the following command to check status.
echo sudo systemctl status callhome-jupiter.service
