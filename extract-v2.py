import os
import pygsheets
import datetime

os.chdir(os.path.dirname(os.path.realpath(__file__)))

# G-sheet init
FILENAME = 'Copy of CallhomeSpectator-VMaaSIT'
WORKSHEET_NAME = 'All CallhomeVM tracker'
bot = pygsheets.authorize(service_file='cred.json')
file = bot.open(FILENAME)
sheet = file.worksheet_by_title(WORKSHEET_NAME)

# Commands used to extract info from Ubuntu machine
commands = {
        "Hostname": "hostname",
        "IP address": "ip route get 1.2.3.4 | awk '{print $7}'",
        "Root Partition": "df -Th /",
        "Storage Blocks": "lsblk",
        "RAM Usage": "free -h",
        "OS Information": "cat /etc/os-release",
        "Hardware Chassis vendor": "cat /sys/class/dmi/id/chassis_vendor",
        "Hardware Product": "cat /sys/class/dmi/id/product_name",
        "Callhome Port": "",
        "Callhome Name": "",
    }


def getOutput(command):
    ''' Returns command output'''
    return os.popen(command).read().strip()


def removeTrailingNulls(a):
    while not a[-1]:
        a.pop()
    return a
    

def getSheetHeaders():
    first_row = removeTrailingNulls(sheet.get_row(1))
    headers = {}
    for i in range(len(first_row)):
        headers[first_row[i].lower().strip()] = i+1
    return headers


def getUniqueColValues(col):
    ports = list(map(lambda x:x.strip().lower(), sheet.get_col(col)))
    return removeTrailingNulls(ports)


def getRownumToUpdate(item, uniqueCol):
    ports = getUniqueColValues(headers[uniqueCol])
    item = str(item).lower().strip()
    if item in ports:
        print("[+] Updating existing Information...")
        return ports.index(item) + 1
    print("[+] Adding a new row...")
    return len(ports) + 1
    

def getColAlphabetFromNum(num):
    alphabet = ''
    while num > 26:
        alphabet += chr(65 + int((num - 1)/26) - 1)
        num = num - (int((num - 1)/26)) * 26
    alphabet += chr(64 + int(num))
    return alphabet


def updateRow(row, info):
    for item in info:
        cell = getColAlphabetFromNum(headers[item.lower()]) + str(row)
        #print(cell)
        sheet.update_value(cell, info[item])


vm_info = {} # Dictionary that stores the command outputs
print("[+] Gathering required information...")
for cmd in commands:
    output = getOutput(commands[cmd])
    vm_info[cmd] = output
    print('*'*5 + cmd.upper() + '*'*5)
    print(output + '\n\n')


vm_info['Last updated'] = datetime.datetime.now().strftime('%Y-%b-%d %I:%M:%S %p')
vm_info['Callhome Port'] = vm_info['Callhome Port'] if vm_info['Callhome Port'] else '4444(test data)'
vm_info['Callhome Name'] = vm_info['Callhome Name'] if vm_info['Callhome Name'] else 'google(test data)'

print("[+] Reading the sheet...")
headers = getSheetHeaders()
print("[+] Checking the sheet for this VM's entry...")
row = getRownumToUpdate(vm_info['Callhome Port'], 'callhome port')
updateRow(row, vm_info)

